package com.example.contactswithnavigation;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ViewHolder> {
    private LayoutInflater inflater;
    private List<Contact> contactList;
    private Context context;

    public ContactAdapter(Context context, List<Contact> contactList) {
        this.inflater = LayoutInflater.from(context);
        this.contactList = contactList;
        this.context = context;
    }

    @NonNull
    @Override
    public ContactAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.favorite_contact_item, parent, false);
        return new ViewHolder(view);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final ImageView contactImage;
        final ImageView contactCallReceived;
        final ImageView contactCallMade;
        final TextView contactName;
        final TextView contactLastAction;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            contactImage = itemView.findViewById(R.id.contactImage);
            contactName = itemView.findViewById(R.id.contactName);

            contactCallReceived = itemView.findViewById(R.id.call_received_icon);
            contactCallMade = itemView.findViewById(R.id.call_made_icon);

            contactLastAction = itemView.findViewById(R.id.contactLastTime);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Contact contact = contactList.get(position);
        if (contact.getAvatar() != null)
            holder.contactImage.setImageResource(context.getResources().getIdentifier(contact.getAvatar(), "drawable", context.getPackageName()));
        holder.contactName.setText(contact.getName());
        holder.contactLastAction.setText(contact.getLastAction());
    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }
}
