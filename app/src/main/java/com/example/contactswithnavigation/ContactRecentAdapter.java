package com.example.contactswithnavigation;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;

import java.util.List;

public class ContactRecentAdapter extends ContactAdapter {
    public ContactRecentAdapter(Context context, List<Contact> contactList) {
        super(context, contactList);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        holder.contactImage.setVisibility(View.GONE);
        holder.contactCallReceived.setVisibility(View.GONE);
        holder.contactCallMade.setVisibility(View.GONE);
    }
}
