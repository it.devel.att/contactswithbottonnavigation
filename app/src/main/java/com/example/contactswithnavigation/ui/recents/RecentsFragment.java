package com.example.contactswithnavigation.ui.recents;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.contactswithnavigation.Contact;
import com.example.contactswithnavigation.ContactAdapter;
import com.example.contactswithnavigation.ContactRecentAdapter;
import com.example.contactswithnavigation.R;

public class RecentsFragment extends Fragment {
    private final String TAG = this.getClass().getSimpleName();

    private Context context;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_recents, container, false);

        RecyclerView contactListRecycleView = root.findViewById(R.id.recentContactList);
        ContactAdapter contactAdapter = new ContactRecentAdapter(context, Contact.generateContacts());
        contactListRecycleView.setAdapter(contactAdapter);

        return root;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.context = null;
    }
}