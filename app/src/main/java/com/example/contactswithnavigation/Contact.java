package com.example.contactswithnavigation;

import java.util.ArrayList;
import java.util.List;

public class Contact {
    private String name;
    private String avatar;
    private String lastAction;

    public Contact(String name, String lastAction) {
        this.name = name;
        this.lastAction = lastAction;
    }

    public Contact(String name, String lastAction, String avatar) {
        this.name = name;
        this.lastAction = lastAction;
        this.avatar = avatar;
    }

    public String getName() {
        return name;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getLastAction() {
        return lastAction;
    }

    public static List<Contact> generateContacts() {
        List<Contact> contactList = new ArrayList<>();
        for (int i = 0; i < 15; i++) {
            String name = String.format("Contact name %s", i + 1);
            String lastAction = "Work, 2hr. ago";

            if (i % 2 == 0){
                contactList.add(new Contact(name, lastAction));
            } else {
                contactList.add(new Contact(name, lastAction, "face_1"));
            }
        }
        return contactList;
    }
}
